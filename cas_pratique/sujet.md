# Sujet exercice Templating

Le but est qu'à partir des fichiers se trouvant dans le dossier `variables`, vous devez écrire le script et le fichier de template original qui permettent de générer les fichiers se trouvant dans le dossier `out`

## HTML

liste des variables:

```conf
    title="Hello There",
    logo_path="../assets/images/logo.png",
    css_path="../assets/css/style.css",
```

## Nginx Conf - json

fichier de variables:

```json
{
    "website": "formation.kyabe-sama.fr",
    "destination_ip": "12.168.111.5",
    "headers": [
        "X-Content-Type-Options nosniff",
        "X-XSS-Protection \"1; mode=block\"",
        "Strict-Transport-Security \"max-age=15552000; includeSubdomains; preload;\"",
        "Referrer-Policy \"no-referrer, strict-origin-when-cross-origin\""
    ]
}
```

## INI Conf - yaml

fichier de variable:

```yaml
subnets:
  - name: "local"
    ip: 192.168.10.10
    gateway: 192.168.10.254
    dns: 192.168.10.253
    interface_name: eth0
  - name: "public"
    ip: 10.0.1.5
    gateway: 10.0.1.1
    dns: 10.0.1.2
    interface_name: ens32
```
