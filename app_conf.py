from jinja2 import Environment, PackageLoader, select_autoescape
import json

env = Environment(loader=PackageLoader("app_conf"), autoescape=select_autoescape())

confTemplate = env.get_template("default-site.j2.conf")

path_to_conf = "out/default-site.conf"
f = open("variables\default-site.json", "r")

# Reading from file
variables = json.loads(f.read())

confContent = confTemplate.render(variables)

with open(path_to_conf, mode="w", encoding="utf-8") as message:
    message.write(confContent)
    print(f"... wrote {path_to_conf}")

# Closing file
f.close()
