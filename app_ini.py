from jinja2 import Environment, PackageLoader, select_autoescape
import yaml
from yaml.loader import SafeLoader

env = Environment(loader=PackageLoader("app_ini"), autoescape=select_autoescape())

iniTemplate = env.get_template("ifcfg-eth0.j2.ini")

path_to_ini = "out/ifcfg-eth0.ini"
f = open("variables\hosts.yaml", "r")

# Reading from file
variables = yaml.load(f, Loader=SafeLoader)

iniContent = iniTemplate.render(variables)

with open(path_to_ini, mode="w", encoding="utf-8") as message:
    message.write(iniContent)
    print(f"... wrote {path_to_ini}")

# Closing file
f.close()
