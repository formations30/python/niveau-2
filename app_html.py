from jinja2 import Environment, PackageLoader, select_autoescape

env = Environment(loader=PackageLoader("app_html"), autoescape=select_autoescape())

htmlTemplate = env.get_template("index.j2.html")

path_to_html = "out/index.html"

htmlContent = htmlTemplate.render(
    title="Hello There",
    logo_path="../assets/images/logo.png",
    css_path="../assets/css/style.css",
)

with open(path_to_html, mode="w", encoding="utf-8") as message:
    message.write(htmlContent)
    print(f"... wrote {path_to_html}")
